""" Ejercicio 11
Escribir un programa que reciba una cadena de caracteres y devuelva un diccionario
con cada palabra que contiene y su frecuencia. Escribir otra función que reciba el
diccionario generado con la función anterior y devuelva una tupla con la palabra
más repetida y su frecuencia. """

def string(string):
    words = {}
    for item in string.split():
        words[item] = string.split().count(item)
    return words

def most_frequent_word(dictionary):
    most_frequent = [max(dictionary, key=dictionary.get), dictionary[max(dictionary, key=dictionary.get)]]
    return tuple(most_frequent)

print(string('Como quieres que te quiera si el que quiero que me quiera no me quiere como quiero que me quiera'))
print(most_frequent_word(string('Como quieres que te quiera si el que quiero que me quiera no me quiere como quiero que me quiera')))