""" Ejercicio 8
Escribir una función que reciba una muestra de números en una lista
y devuelva un diccionario con su media, varianza y desviación típica. """
import math

def statistics(list):
    average = sum(list)/len(list)
    variance = sum(math.pow(item-average, 2) for item in list)/len(list)
    deviation = math.sqrt(variance)
    return {'media':average,'Varianza':variance,'Desviación':deviation}

print(statistics([1, 2, 3, 4, 5]))
print(statistics([2.3, 5.7, 6.8, 9.7, 12.1, 15.6]))