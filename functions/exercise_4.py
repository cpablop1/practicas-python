""" Ejercicio 4
Escribir una función que calcule el total de una factura tras aplicarle el IVA.
La función debe recibir la cantidad sin IVA y el porcentaje de IVA a aplicar, y
devolver el total de la factura. Si se invoca la función sin pasarle el
porcentaje de IVA, deberá aplicar un 21%. """

def invoices(amount, iva=0.21):
    total = (amount*iva) + amount
    return total

print(invoices(1000,10))
print(invoices(1000))