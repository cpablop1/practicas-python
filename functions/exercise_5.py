""" Ejercicio 5
Escribir una función que calcule el área de un círculo y otra
que calcule el volumen de un cilindro usando la primera función. """
import math

def circle_area(radius):
    radius = math.pi*pow(radius, 2)
    return round(radius, 2)

def volume_cylinder(radius, height):
    volume = circle_area(radius)*height
    return volume

print(volume_cylinder(3,10))