""" Ejercicio 10
Escribir una función que convierta un número decimal en binario
y otra que convierta un número binario en decimal. """

def decimal_to_binary(number):
    binary = []
    while number!=0:
        binary.append(str(number%2))
        number //= 2
    binary.reverse()
    return ''.join(binary)

def binary_to_decimal(number):
    number = number[::-1]
    decimal = 0
    for item in range(len(number)):
        decimal += (int(number[item])*(2)**item)
    return decimal

print(binary_to_decimal('10110'))
print(decimal_to_binary(22))
print(binary_to_decimal(decimal_to_binary(22)))
print(decimal_to_binary(binary_to_decimal('10110')))