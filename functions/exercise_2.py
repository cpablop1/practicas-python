""" Ejercicio 2
Escribir una función a la que se le pase una cadena <nombre>
y muestre por pantalla el saludo ¡hola <nombre>!. """

def greet(name):
    print(f'¡Hola {name}!')
    return

greet('Pablo')
greet('María')