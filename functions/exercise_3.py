""" Ejercicio 3
Escribir una función que reciba un número entero positivo y devuelva su factorial. """

def factorial(number):
    factorial = 1
    for item in range(1, number+1):
        factorial *= item
    print(f'El factorial de {number} es {factorial}')
    return

factorial(5)
factorial(10)