""" Ejercicio 6
Escribir una función que reciba una muestra de números en una lista y devuelva su media. """

def average(list):
    list = sum(list)/len(list)
    return list

print(average([1, 2, 3, 4, 5]))
print(average([2.3, 5.7, 6.8, 9.7, 12.1, 15.6]))