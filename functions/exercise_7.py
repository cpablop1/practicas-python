""" Ejercicio 7
Escribir una función que reciba una muestra de números en
una lista y devuelva otra lista con sus cuadrados. """

def square_number(list):
    for item in range(len(list)):
        list[item] = list[item]**2
    return list

print(square_number([1, 2, 3, 4, 5]))
print(square_number([2.3, 5.7, 6.8, 9.7, 12.1, 15.6]))