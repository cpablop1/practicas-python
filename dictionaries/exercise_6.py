""" Ejercicio 6
Escribir un programa que cree un diccionario vacío y lo vaya llenado con información
sobre una persona (por ejemplo nombre, edad, sexo, teléfono, correo electrónico, etc.)
que se le pida al usuario. Cada vez que se añada un nuevo dato debe imprimirse el
contenido del diccionario. """

def personal_information():
    info = {}
    next = True
    while next:
        key = input('¿Qué dato quieres agregar: ').title()
        value = input(f'{key}: ').title()
        info[key] = value
        print(info)
        next = input('¿Quieres seguir agregando datos (Si/No)? ').title() == 'Si'

personal_information()