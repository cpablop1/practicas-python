""" Ejercicio 4
Escribir un programa que pregunte una fecha en formato dd/mm/aaaa
y muestre por pantalla la misma fecha en formato dd de <mes> de
aaaa donde <mes> es el nombre del mes. """
#import locale
#from datetime import datetime

#locale.setlocale(locale.LC_TIME, ('es_ES.UTF-8'))

def date():
    #date = input('Hola ingrese una fecha con el siguiente formato: dd/mm/aaaa ').split('/')
    #date = datetime(int(date[2]), int(date[1]), int(date[0]))
    #print(date.strftime('\n%d de %B de %Y\n'))
    month = {1:'enero',2:'febrero',3:'marzo',4:'abril',5:'mayo',6:'junio',7:'julio',8:'agosto',9:'septiembre',10:'octubre',11:'noviembre',12:'diciembre'}
    date = input('Hola ingrese una fecha con el siguiente formato: dd/mm/aaaa ').split('/')
    date = [int(item) for item in date]
    print(f'{date[0]} de {month[date[1]]} de {date[2]}')

date()