""" Ejercicio 8
Escribir un programa que cree un diccionario de traducción español-inglés. El usuario
introducirá las palabras en español e inglés separadas por dos puntos, y cada par
<palabra>:<traducción> separados por comas. El programa debe crear un diccionario con
las palabras y sus traducciones. Después pedirá una frase en español y utilizará el
diccionario para traducirla palabra a palabra. Si una palabra no está en el diccionario
debe dejarla sin traducir. """

def translator():
    dictionary = {}
    print('\nIngrese las palabras y su traducción en inglés.')
    add = True
    while add:
        word = input('Con el siguente formato <palabra>:<traducción> ').split(':')
        if word[0] == 'salir':
            add = False
        else:
            dictionary[word[0]] = word[1]
    phrase = input('Ahora ingrese una frase en español: ')
    for key, value in dictionary.items():
        phrase = phrase.replace(key, value)
    print(phrase.capitalize())

translator()