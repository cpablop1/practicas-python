""" Ejercicio 2
Escribir un programa que pregunte al usuario su nombre, edad,
dirección y teléfono y lo guarde en un diccionario. Después
debe mostrar por pantalla el mensaje <nombre> tiene <edad>
años, vive en <dirección> y su número de teléfono es <teléfono>. """

def personal_information():
    info = {}
    name = input('Hola, ingrese su nombre: ')
    age = input('Ahora ingrese su edad: ')
    address = input('Bien, ahora su dirección: ')
    phone = input('Por último ingrese su tuléfono: ')

    info['name'] = name
    info['age'] = age
    info['address'] = address
    info['phone'] = phone

    print(f'\n{info.get("name")} tiene {info.get("age")} años, vive en {info.get("address")}' +
          f' y\nsu número de teléfono es {info.get("phone")}\n')

personal_information()