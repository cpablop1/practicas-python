""" Ejercicio 5
Escribir un programa que almacene el diccionario con los créditos de
las asignaturas de un curso {'Matemáticas': 6, 'Física': 4, 'Química': 5}
y después muestre por pantalla los créditos de cada asignatura en el
formato <asignatura> tiene <créditos> créditos, donde <asignatura> es
cada una de las asignaturas del curso, y <créditos> son sus créditos.
Al final debe mostrar también el número total de créditos del curso. """

def subjects():
    subjects = {'Matemáticas': 6, 'Física': 4, 'Química': 5}
    credits = 0
    print()
    """ for item in subjects: Con forma solo retorna la clave, con la clave accedo al valor
        credits += subjects[item]
        print(f'{item} tiene {subjects[item]} créditos.') """
    for sub, cre in subjects.items(): # En cambio en ésta me retorna tanto el valor como la clave
        credits += cre
        print(f'{sub} tiene {cre} créditos.')
    print('--------------------------------')
    print(f'Total de créditos es {credits}\n')

subjects()