""" Ejercicio 3
Escribir un programa que guarde en un diccionario los precios de
las frutas de la tabla, pregunte al usuario por una fruta, un
número de kilos y muestre por pantalla el precio de ese número
de kilos de fruta. Si la fruta no está en el diccionario debe
mostrar un mensaje informando de ello.

Fruta	Precio
Plátano	1.35
Manzana	0.80
Pera	0.85
Naranja	0.70 """

def sales():
    fruit_price = {'Plátano':1.35,'Manzana':0.80,'Pera':0.85,'Naranja':0.70}
    fruit = input('Hola, ingrese la fruta que desee: ').title()
    kilos = int(input('¿Cuántos kilos necesita? '))
    total = 0
    if fruit in fruit_price:
        total = kilos * fruit_price.get(fruit)
        print('\nCada kilo de {} vale Q. {:.2f}'.format(fruit,fruit_price[fruit]))
        print('El total a pagar por {} kilos es Q. {:.2f}\n'.format(str(kilos), total))
    else:
        print('Lo siento, el producto que desee no está en la lista!')

sales()