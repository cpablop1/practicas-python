""" Ejercicio 10
Escribir un programa que permita gestionar la base de datos de clientes de una empresa.
Los clientes se guardarán en un diccionario en el que la clave de cada cliente será su
NIF, y el valor será otro diccionario con los datos del cliente (nombre, dirección,
teléfono, correo, preferente), donde preferente tendrá el valor True si se trata de un
cliente preferente. El programa debe preguntar al usuario por una opción del siguiente
menú: (1) Añadir cliente, (2) Eliminar cliente, (3) Mostrar cliente, (4) Listar todos
los clientes, (5) Listar clientes preferentes, (6) Terminar. En función de la opción
elegida el programa tendrá que hacer lo siguiente:

1. Preguntar los datos del cliente, crear un diccionario con los datos y añadirlo
a la base de datos.
2. Preguntar por el NIF del cliente y eliminar sus datos de la base de datos.
3. Preguntar por el NIF del cliente y mostrar sus datos.
4. Mostrar lista de todos los clientes de la base datos con su NIF y nombre.
5. Mostrar la lista de clientes preferentes de la base de datos con su NIF y nombre.
6. Terminar el programa.
    https://aprendeconalf.es/docencia/python/ejercicios/diccionarios/
 """

def database_manager():
    database = {}
    option = 0
    while option != 6:
        option = int(input('\n---------- Menú ----------' +
                           '\n1 Añadir Cliente' +
                           '\n2 Eliminar Cliente' +
                           '\n3 Mostrar Cliente' +
                           '\n4 Listar Todos Los Clientes' +
                           '\n5 Listar Clientes Preferentes' +
                           '\n6 Salir' +
                           '\nIngrese el número de la acción: '))
        if option == 1:
            key = input('\nIngrese NIF: ')
            value = {
                'name': input('Ingrese nombre: ').title(),
                'address': input('Ingrese dirección: ').title(),
                'phone': input('Ingrese número telefónico: '),
                'email': input('Ingrese correo electrónico: '),
                'preferred': input('Es un cliente preferente (Sí/No): ').title() == 'Si'
            }
            database[key] = value
        elif option == 2:
            key = input('¿Cuál es el NIF del cliente? ')
            if database.get(key) != None:
                del database[key]
                print(f'\nEl cliente con el NIF {key} se ha eliminado correctamente.')
            else:
                print(f'\nNo existe ningún cliente con NIF {key}')
        elif option == 3:
            key = input('¿Cuál es el NIF del cliente? ')
            if database.get(key) != None:
                print(database[key])
            else:
                print(f'\nNo existe ningún cliente con NIF {key}')
        elif option == 4:
            print('\n----------------------------------')
            for key, value in database.items():
                print(f'NIF: {key}')
                print('{:4}{:15}{}'.format('','Nombre:',value['name']))
                print('{:4}{:15}{}'.format('','Dirección:',value['address']))
                print('{:4}{:15}{}'.format('','Teléfono:',value['phone']))
                print('{:4}{:15}{}'.format('','Correo:',value['email']))
                print('{:4}{:15}{}'.format('','Preferente:', 'Sí' if value['preferred'] else 'No'))
                print('----------------------------------')
        elif option == 5:
            print('\n----------------------------------')
            for key, value in database.items():
                if value['preferred']:
                    print(f'NIF: {key}')
                    print('{:4}{:15}{}'.format('','Nombre:',value['name']))
                    print('{:4}{:15}{}'.format('','Dirección:',value['address']))
                    print('{:4}{:15}{}'.format('','Teléfono:',value['phone']))
                    print('{:4}{:15}{}'.format('','Correo:',value['email']))
                    print('{:4}{:15}{}'.format('','Preferente:', 'Sí' if value['preferred'] else 'No'))
                    print('----------------------------------')
    print('\n-----------Sistema Cerrado -----------\n')
        
database_manager()