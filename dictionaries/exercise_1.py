""" Ejercicio 1
Escribir un programa que guarde en una variable el diccionario
{'Euro':'€', 'Dollar':'$', 'Yen':'¥'}, pregunte al usuario
por una divisa y muestre su símbolo o un mensaje de aviso si
la divisa no está en el diccionario. """

def badge():
    dic_badge = {'Euro':'€', 'Dollar':'$', 'Yen':'¥'}
    badge = input('Hola, ingrese la divisa a consultar: ')
    badge = badge.title()
    if badge in dic_badge:
        print(f'El síbolo de la divisa {badge} es {dic_badge[badge]}.')
    else:
        print(f'El {badge} no está en la lista.')

badge()