""" Ejercicio 9
Escribir un programa que gestione las facturas pendientes de cobro de una empresa.
Las facturas se almacenarán en un diccionario donde la clave de cada factura será
el número de factura y el valor el coste de la factura. El programa debe preguntar
al usuario si quiere añadir una nueva factura, pagar una existente o terminar. Si
desea añadir una nueva factura se preguntará por el número de factura y su coste y
se añadirá al diccionario. Si se desea pagar una factura se preguntará por el
número de factura y se eliminará del diccionario. Después de cada operación el
programa debe mostrar por pantalla la cantidad cobrada hasta el momento y la
cantidad pendiente de cobro. """

def invoices():
    invoice = {}
    num_invoice = cost = option = 0
    collected = pending = 0
    while option != 3:
        pending = sum(list((value for key,value in invoice.items())))
        print(f'\nFacturas: {invoice}')
        print('Pendiente de cobro {:.2f}'.format(pending))
        print('Recaudados: {:.2f}'.format(collected))
        option = int(input('\n1 - Añadir Factura' +
                    '\n2 - Pagar Factura' +
                    '\n3 - Salir'+
                    '\nIngrese el número de la opción: '))
        if option == 1:
            num_invoice = int(input('Ingrese el número de factura: '))
            cost = float(input(f'¿Cuál es el coste de la factura {str(num_invoice)}? '))
            invoice[num_invoice] = cost
        elif option == 2:
            num_invoice = int(input('¿Cuál es el número de factura: '))
            if invoice.get(num_invoice) != None:
                collected += invoice[num_invoice]
                del invoice[num_invoice]
            else:
                print(f'\nLa factura {str(num_invoice)} es inválido.')
    
invoices()