""" Ejercicio 7
Escribir un programa que cree un diccionario simulando una cesta de la compra. El
programa debe preguntar el artículo y su precio y añadir el par al diccionario, hasta
que el usuario decida terminar. Después se debe mostrar por pantalla la lista de la
compra y el coste total, con el siguiente formato

Lista de la compra	
Artículo 1	    Precio
Artículo 2	    Precio
Artículo 3	    Precio
…	            …
Total	        Coste """

def shopping_cart():
    cart = {}
    cost = 0.0
    add = True
    while add:
        article = input('Ingrese nombre del artículo: ').title()
        if article == 'Salir':
            add = False
        else:
            price = float(input(f'Precio del {article}: '))
            cart[article] = price
    print('\nLista de la compra.')
    for article, price in cart.items():
        cost += price
        print('{:15} Q.{:.2f}'.format(article,price))
    print('{:15} {}'.format('------','------'))
    print('{:15} Q.{:.2f}\n'.format('Total',cost))

shopping_cart()