""" Ejercicio 2
Escribir un programa que pregunte al usuario su edad y
muestre por pantalla todos los años que ha cumplido
(desde 1 hasta su edad). """

def year():
    year = input('Hola, ingrese tu edad: ')
    while is_valid(year) != True:
        year = input('La edad debe ser un número (56, 20, 15 ...) ')
    for item in range(int(year)):
        print(f'Has cumplido {item+1} años de edad😀')

def is_valid(year):
    try:
        int(year)
        return True
    except:
        return False
    
year()