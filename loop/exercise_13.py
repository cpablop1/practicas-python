""" Ejercicio 13
Escribir un programa que muestre el eco de todo lo que el usuario
introduzca hasta que el usuario escriba “salir” que terminará. """


def echo():
    text = input('Hola, escribe algo: ')
    while text != 'salir':
        print(text)
        text = input('Hola, escribe algo: ')

echo()