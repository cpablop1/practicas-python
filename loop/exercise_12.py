""" Ejercicio 12
Escribir un programa en el que se pregunte al usuario por una frase y
una letra, y muestre por pantalla el número de veces que aparece la
letra en la frase. """

def count_letter():
    phase = input('Hola, escribe una frase: ')
    letter = input('Muy bien, ahora ingrese una letra: ')
    count_letter = 0
    for item in phase:
        if item == letter:
            count_letter += 1
    print(f'\nLa letra "{letter}" aparece {str(count_letter)} veces en la frase.')

count_letter()