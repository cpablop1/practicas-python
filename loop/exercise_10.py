""" Ejercicio 10
Escribir un programa que pida al usuario un número entero
y muestre por pantalla si es un número primo o no. """

def main():
    number = input('Hola, ingrese un número: ')
    while is_int(number)!=True:
        number = input('Debe ingresar un número entero (Eje: 5, 9, 7, 25...) ')
    if is_prime_number(number):
        print('Es primo')
    else:
        print('No es primo')

def is_prime_number(number):
    for item in range(2, int(number)):
        if int(number)%item == 0:
            return False
    return True

def is_int(number):
    try:
        int(number)
        return True
    except:
        return False

main()