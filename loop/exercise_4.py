""" Ejercicio 4
Escribir un programa que pida al usuario un número entero positivo
y muestre por pantalla la cuenta atrás desde ese número hasta cero
separados por comas. """

def reverse():
    number = input('Hola, ingrese un número: ')
    while is_valid(number) != True:
        number = input('Debe ingresar un número entero (Eje: 5, 45, 3...): ')
    for item in range(int(number), -1, -1):
        print(item, end=', ')

def is_valid(number):
    try:
        int(number)
        return True
    except:
        return False
    
reverse()