""" Ejercicio 7
Escribir un programa que muestre por pantalla la tabla de multiplicar del 1 al 10. """

def multiplication_table():
    for multiplier in range(1, 11):
        print(f'\n##### Tabla del {multiplier} #####')
        for multiplying in range(1, 11):
            print(f'{multiplier} X {multiplying} = {multiplier*multiplying}')

multiplication_table()