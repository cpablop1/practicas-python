""" Ejercicio 6
Escribir un programa que pida al usuario un número entero y muestre
por pantalla un triángulo rectángulo como el de más abajo, de altura
el número introducido. 

*
**
***
****
***** """

def right_triangle():
    number = input('Hola, ingrese un número: ')
    while is_int(number)!=True:
        number = input('Debe ingresar un número entero (Eje: 5, 6, 15, 3 ...) ')
    for item in range(int(number)+1):
        print('*'*item)

def is_int(number):
    try:
        int(number)
        return True
    except:
        return False
    
right_triangle()