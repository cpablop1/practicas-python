""" Ejercicio 3
Escribir un programa que pida al usuario un número entero positivo
y muestre por pantalla todos los números impares desde 1 hasta
ese número separados por comas. """

def odd_numbers():
    number = input('Hola, ingrese un número: ')
    while is_valid(number)!=True:
        number = input('Debes ingresar un número entero (Eje: 5, 25, 10...): ')
    for item in range(1, int(number)+1, 2):
        print(item, end=', ')

def is_valid(number):
    try:
        int(number)
        return True
    except:
        return False
    
odd_numbers()