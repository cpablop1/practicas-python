""" Ejercicio 5
Escribir un programa que pregunte al usuario una cantidad a invertir,
el interés anual y el número de años, y muestre por pantalla el capital
obtenido en la inversión cada año que dura la inversión. """

def investment():
    investment = input('Hola, ingrese la cantidad a invertir: ')
    interest = input('Ahora ingrese el interés anual: ')
    year = input('Finalmente ingrese la cantidad de años que dure su inversión: ')
    condition = [is_float(investment), is_float(interest), is_int(year)]
    while all(condition) != True:
        if condition.index(False) == 0:
            investment = input('Ingrese una cantidad a invertir válida (Eje: 1500, 300, 5000...) ')
        elif condition.index(False) == 1:
            interest = input('Ingrese un interés anual válida (Eje: 0.5, 0.12, 0.04 ...) ')
        else:
            year = input('Ingrese un año válido (Eje: 5, 9, 10, 15 ...) ')
        condition = [is_float(investment), is_float(interest), is_int(year)]
    for i in range(int(year)):
        investment = float(investment)
        investment *= 1 + float(interest) / 100 
        print("Capital tras " + str(i+1) + " años: " + str(round(investment, 2)))

def is_float(number):
    try:
        float(number)
        return True
    except:
        return False

def is_int(number):
    try:
        int(number)
        return True
    except:
        return False
    
investment()