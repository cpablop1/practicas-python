""" Ejercicio 8
Escribir un programa que pida al usuario un número entero y muestre
por pantalla un triángulo rectángulo como el de más abajo.

1
3 1
5 3 1
7 5 3 1
9 7 5 3 1 """

def right_triangle():
    number = input('Hola, ingresa un número: ')
    while is_int(number)!=True:
        number = input('Debe ingresar un número entero (Eje: 5, 89, 7...) ')
    for i in range(1, int(number)+1, 2):
        for j in range(i, 0, -2):
            print(j, end=' ')
        print()

def is_int(number):
    try:
        int(number)
        return True
    except:
        return False
    
right_triangle()