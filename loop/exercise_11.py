""" Ejercicio 11
Escribir un programa que pida al usuario una palabra y luego muestre
por pantalla una a una las letras de la palabra introducida empezando
por la última. """

def reverse():
    text = input('Hola, ingresa una palabra: ')
    for item in range(len(text)-1, -1, -1):
        print(text[item])

reverse()