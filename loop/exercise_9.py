""" Ejercicio 9
Escribir un programa que almacene la cadena de caracteres contraseña
en una variable, pregunte al usuario por la contraseña hasta que
introduzca la contraseña correcta. """

def password():
    PASSWORD = 'pablo'
    password = input('Hola, ingresa la contraseña: ')
    while password != PASSWORD:
        password = input('Contraseña incorrecta, vuelve a intentarlo: ')
    print('\nMuy bien, has ingresado la contraseña correcta!\n')

password()