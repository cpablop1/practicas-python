""" Ejercicio 4
Escribir un programa que pida al usuario un número entero y 
muestre por pantalla si es par o impar. """

def main():
    number = input('Hola, ingrese un número entero: ')
    while is_valid(number)!=True:
        number = input('El número debe ser un entero: ')
    if int(number)%2 == 0:
        print('Has ingresado un número par.')
    else:
        print('El número es impar.')

def is_valid(number):
    try:
        int(number)
        return True
    except:
        return False
    
main()