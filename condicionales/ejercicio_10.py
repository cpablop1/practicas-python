""" Ejercicio 10
La pizzería Bella Napoli ofrece pizzas vegetarianas y no vegetarianas a
sus clientes. Los ingredientes para cada tipo de pizza aparecen a continuación.

- Ingredientes vegetarianos: Pimiento y tofu.
- Ingredientes no vegetarianos: Peperoni, Jamón y Salmón.

Escribir un programa que pregunte al usuario si quiere una pizza vegetariana o
no, y en función de su respuesta le muestre un menú con los ingredientes
disponibles para que elija. Solo se puede eligir un ingrediente además de la
mozzarella y el tomate que están en todas la pizzas. Al final se debe mostrar
por pantalla si la pizza elegida es vegetariana o no y todos los ingredientes
que lleva. """

def pizza():
    print('\nHola, te ofrecemos las siguientes pizzas:')
    print('🔸 (1)Pizzas Vegetarianas')
    print('🔸 (2)Pizzas No Vegetarianas')
    pizza = input('Ingrese el número de la pizza: ')
    while is_valid(pizza, range(1, 3))!=True:
        pizza = input('Ingrese el número de la pizza: ')
    if int(pizza) == 1:
        list_ingre = ['1 - Pimiento', '2 - Tofu']
        print('\nExcelente, has seleccionado una pizza vegetariana')
        print('Tenemos de los siguentes:')
        print('\n'.join(list_ingre))
        ingredients = input('Seleccione el ingrediente para tu pizza: ')
        while is_valid(ingredients, range(1, 3))!=True:
            ingredients = input('Porfavor ingrese 1 o 2: ')
        print('\n--------- Orden de Pizza Vegetariana ---------')
        print('Ingredientes:')
        print('- Mozzarella')
        print('- Tomate')
        print(f'- {list_ingre[int(ingredients)-1].split(" ")[2]}')
    else:
        list_ingre = ['1 - Peperoni', '2 - Jamón', '3 - Salmón']
        print('\nExcelente, has seleccionado una pizza convencional')
        print('Tenemos de los siguentes:')
        print('\n'.join(list_ingre))
        ingredients = input('Seleccione el ingrediente para tu pizza: ')
        while is_valid(ingredients, range(1, 4))!=True:
            ingredients = input('Porfavor ingrese 1, 2 o 3: ')
        print('\n--------- Orden de Pizza Convencional ---------')
        print('Ingredientes:')
        print('- Mozzarella')
        print('- Tomate')
        print(f'- {list_ingre[int(ingredients)-1].split(" ")[2]}')

def is_valid(number, range):
    try:
        int(number)
        if int(number) in range:
            return True
        else:
            return False
    except:
        return False

pizza()