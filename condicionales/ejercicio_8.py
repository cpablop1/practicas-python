""" Ejercicio 8
En una determinada empresa, sus empleados son evaluados al final de cada año.
Los puntos que pueden obtener en la evaluación comienzan en 0.0 y pueden ir
aumentando, traduciéndose en mejores beneficios. Los puntos que pueden conseguir
los empleados pueden ser 0.0, 0.4, 0.6 o más, pero no valores intermedios entre
las cifras mencionadas. A continuación se muestra una tabla con los niveles
correspondientes a cada puntuación. La cantidad de dinero conseguida en cada nivel
es de 2.400€ multiplicada por la puntuación del nivel.

    Nivel	        Puntuación
Inaceptable	            0.0
Aceptable	            0.4
Meritorio	            0.6 o más
Escribir un programa que lea la puntuación del usuario e indique su nivel de rendimiento,
así como la cantidad de dinero que recibirá el usuario. """

def score():
    MONEY = 2.00
    money_score = 0.00
    level = ''
    score = input('Hola, ingrese la puntuación del usuario: ')
    while is_valid(score)!=True:
        score = input('La puntuación debe ser un número (Eje: 0, 0.4, 0.6 o más) ')
    score = float(score)
    if score == 0:
        money_score = MONEY + (score * MONEY)
        level = 'INACEPTABLE'
    elif score == 0.4:
        money_score = MONEY + (score * MONEY)
        level = 'ACEPTABLE'
    elif score >= 0.6:
        money_score = MONEY + (score * MONEY)
        level = 'MERITORIO'
    print('Su nivel es {} y la cantidad que recibirá es de Q.{:.2f}'.format(level, money_score))

def is_valid(number):
    try:
        float(number)
        if int(number.split('.')[1])%2 == 0:
            return True
        else:
            return False
    except:
        return False
    
score()