""" Ejercicio 6
Los alumnos de un curso se han dividido en dos grupos A y B de acuerdo al
sexo y el nombre. El grupo A esta formado por las mujeres con un nombre
anterior a la M y los hombres con un nombre posterior a la N y el grupo B
por el resto. Escribir un programa que pregunte al usuario su nombre y sexo,
y muestre por pantalla el grupo que le corresponde. """
import re

def group():
    name = input('Hola, ingrese su nombre: ')
    sex = input('Muy bien, ahora ingrese su género (Masculino (m), Femenino (f)): ')
    condition = [valid_name(name), valid_sex(sex)]
    while all(condition)!=True: # el metodo all() devuelve True si todos los valores son True
        if condition[0]!=True:
            name = input('Ingrese un nombre válido: ')
        elif condition[1]!=True:
            sex = input('Para el género ingrese m si Hombre y f si es mujer: ')
        condition = [valid_name(name), valid_sex(sex)]
    if (name[0].lower() < 'm') & (sex.lower() == 'f'):
        print(f'Hola {name}, perteneces al Grupo A')
    elif (name[0].lower() > 'n') & (sex.lower() == 'm'):
        print(f'Hola {name}, perteneces al Grupo A')
    else:
        print(f'Hola {name}, perteneces al Grupo B')

def valid_name(name):
    pattern = re.compile(r"^[a-zA-Z]+(?: [a-zA-Z]+)*$")
    if pattern.match(name):
        return True
    else:
        return False

def valid_sex(sex):
    sex = sex.lower()
    if (sex == 'm') | (sex == 'f'):
        return True
    else:
        return False
    
group()