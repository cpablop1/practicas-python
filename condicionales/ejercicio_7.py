""" Ejercicio 7
Los tramos impositivos para la declaración de la renta en un determinado país son los siguientes:

        Renta	        Tipo impositivo
Menos de 10000Q	                5%
Entre 10000€ y 20000Q	        15%
Entre 20000€ y 35000Q	        20%
Entre 35000€ y 60000Q	        30%
Más de 60000Q	                45%
Escribir un programa que pregunte al usuario su renta anual y muestre por pantalla
el tipo impositivo que le corresponde. """

def tax():
    tax = 0
    rent = input('Hola, ingrese su renta anual: ')
    while is_valid(rent)!=True:
        rent = input('La renta debe ser una cantidad (Eje: 1500, 5000 ...) ')
    rent = float(rent)
    if rent in range(0, 10000+1):
        tax = 5
    elif rent in range(10000, 20000+1):
        tax = 15
    elif rent in range(20000, 35000+1):
        tax = 20
    elif rent in range(35000, 60000+1):
        tax = 30
    elif rent > 60000:
        tax = 45
    print('\nLa cantidad de Q.{:.2f} le corrsponde el {}%\npor lo que son Q.{:.2f} de impositivo.\n'.format(rent, tax, rent*(tax/100)))

def is_valid(number):
    try:
        int(float(number))
        return True
    except:
        return False
    
tax()