""" Ejercicio 3
Escribir un programa que pida al usuario dos números y muestre por pantalla su división.
Si el divisor es cero el programa debe mostrar un error. """

def division():
    print('##### Sistema para dividir #####' + '\n')
    dividend = input('Ingrese el dividiendo: ')
    divider = input('Bien, ahora ingrese el divisor: ')
    is_zero = False
    while ((is_valid(dividend)!=True) | (is_valid(divider)!=True)) & is_zero!=True:
        if is_valid(dividend)!=True:
            dividend = input('El dividiendo debe ser un número: ')
        elif is_valid(divider)!=True:
            divider = input('El divisor debe ser un número: ')
        elif float(divider) == 0:
            divider = input('El divisor debe ser mayor a cero: ')
            is_zero = False
        else:
            is_zero = True
            division1 = round(float(dividend) / float(divider), 2)
            print('{:.2f}'.format(division1))
            break

def is_valid(number):
    try:
        int(float(number))
        return True
    except:
        return False
    
division()