""" Ejercicio 5
Para tributar un determinado impuesto se debe ser mayor de 16 años y
tener unos ingresos iguales o superiores a 1000 Q mensuales. Escribir
un programa que pregunte al usuario su edad y sus ingresos mensuales
y muestre por pantalla si el usuario tiene que tributar o no. """

def tax():
    year = input('Hola, ingrese tu edad: ')
    income = input('Muy bien, ahora ingrese su salario: ')
    while (is_valid(year)!=True) | (is_valid(income)!=True):
        if is_valid(year)!=True:
            year = input('La edad debe un número entero (Eje: 15, 25, 86): ')
        elif is_valid(income)!=True:
            income = input('El ingreso debe ser una cantidad (Eje: 1500, 500, 800): ')
    if (int(year) > 16) & (float(income) >= 1000):
        print('Felicidades, cumples los requisitos para tributar.')
    else:
        print('Aun no estas listo para tributar, vuelve pronto.')

def is_valid(number):
    try:
        int(float(number))
        return True
    except:
        return False
    
tax()