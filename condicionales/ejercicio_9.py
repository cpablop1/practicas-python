""" Ejercicio 9
Escribir un programa para una empresa que tiene salas de juegos para todas
las edades y quiere calcular de forma automática el precio que debe cobrar
a sus clientes por entrar. El programa debe preguntar al usuario la edad
del cliente y mostrar el precio de la entrada. Si el cliente es menor de
4 años puede entrar gratis, si tiene entre 4 y 18 años debe pagar 5€ y si
es mayor de 18 años, 10€. """

def play_rooms():
    year = input('Hola, cuál es tu edad? ')
    ticket = 0
    while is_valid(year)!=True:
        year = input('Debes ingresar un número (Eje: 45, 18, 6) ')
    year = int(year)
    if year < 4:
        ticket = ticket
    elif year in range(4, 19):
        ticket = 5
    elif year > 18:
        ticket = 10
    if ticket == 0:
        print('Puedes entrar gratis, disfrútelo!😉')
    else:
        print('Buenvenido, debes cancelar Q.{:.2f} por la entrada, disfrútelo!😀'.format(ticket))
def is_valid(year):
    try:
        int(year)
        return True
    except:
        return False
    
play_rooms()