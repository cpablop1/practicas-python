""" Ejercicio 6
Escribir una función que reciba una lista de notas y devuelva la lista
de calificaciones correspondientes a esas notas. 

A = Aprobrado
R = Reprobado

 """

def main(list):
    for item in range(len(list)):
        list[item] = evaluate(list[item])
    return list

def evaluate(score):
    if score > 59:
        return 'A'
    return 'R'

print(main([56,89,60,78,45]))