""" Ejercicio 1
Escribir una función que aplique un descuento a un precio y otra que aplique el
IVA a un precio. Escribir una tercera función que reciba un diccionario con los
precios y porcentajes de una cesta de la compra, y una de las funciones anteriores,
y utilice la función pasada para aplicar los descuentos o el IVA a los productos
de la cesta y devolver el precio final de la cesta. """
# https://aprendeconalf.es/docencia/python/ejercicios/programacion-funcional/

def discount(price, discount):
    discount = discount/100
    return price - (price*discount)

def iva(price, iva):
    iva = iva/100
    return (price*iva) + price

def main(dictionary, function):
    total = 0
    for price,porcentage in dictionary.items():
        total += function(price,porcentage)
    return total

print('\nEl precio de la compra tras aplicar el descuento es {:.2f}'.format(main({1000:20, 500:10, 100:1}, discount)))
print('El precio de la compra tras aplicar el IVA es {:.2f}\n'.format(main({1000:20, 500:10, 100:1}, iva)))