""" Ejercicio 9
Escribir una función que calcule el módulo de un vector. """

from math import pow, sqrt

def vector_module(vector1,vector2):
    vectors = sqrt(pow(vector1, 2) + pow(vector2, 2))
    return round(vectors, 2)

print(vector_module(-4,7))