""" Ejercicio 2
Escribir una función que simule una calculadora científica que permita calcular
el seno, coseno, tangente, exponencial y logaritmo neperiano. La función preguntará
al usuario el valor y la función a aplicar, y mostrará por pantalla una tabla con
los enteros de 1 al valor introducido y el resultado de aplicar la función a esos enteros. """
from math import cos,sin,tan,exp,log
def scientific_calculator(number, function):
    calculator = {}
    for item in range(1, number+1):
        calculator[item] = round(function(item), 2)
    return calculator

print(f'Coseno {scientific_calculator(5,cos)}')
print(f'Seno {scientific_calculator(5,sin)}')
print(f'Tangente {scientific_calculator(5,tan)}')
print(f'Exponencial {scientific_calculator(5,exp)}')
print(f'Logaritmo {scientific_calculator(5,log)}')