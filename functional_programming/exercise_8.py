""" Ejercicio 8
Escribir una función reciba un diccionario con las asignaturas y las notas de un
alumno y devuelva otro diccionario con las asignaturas en mayúsculas y las
calificaciones correspondientes a las notas aprobadas. """

def main(dictionary):
    new_dict = {}
    for key,value in dictionary.items():
        if value > 59:
            new_dict[key.upper()] = 'A'
    return new_dict

print(main({'matematica':89,'ciencias':78,'fisica':56}))