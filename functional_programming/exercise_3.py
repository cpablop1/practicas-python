""" Ejercicio 3
Escribir una función que reciba otra función y una lista, y devuelva otra lista
con el resultado de aplicar la función dada a cada uno de los elementos de la lista. """

# https://aprendeconalf.es/docencia/python/ejercicios/programacion-funcional/

def main(function, list):
    value = function(list)
    return value

def square(list):
    for item in range(len(list)):
        list[item] = int(list[item])**item
    return list

print(main(square, [2,3,4,5,6]))