""" Ejercicio 7
Escribir una función reciba un diccionario con las asignaturas y las notas
de un alumno y devuelva otro diccionario con las asignaturas en mayúsculas
y las calificaciones correspondientes a las notas. """

def objects(dictionary):
    new_dict = {}
    for key, value in dictionary.items():
        new_dict[key.upper()] = evaluate(value)
    return new_dict

def evaluate(score):
    if score > 59:
        return 'A'
    return 'R'

print(objects({'matematica':89,'ciencias':78,'fisica':56}))