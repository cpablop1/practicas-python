""" Ejercicio 11
Escribir una función que reciba una muestra de números y devuelva los valores
atípicos, es decir, los valores cuya puntuación típica sea mayor que 3 o menor
que -3. Nota: La puntuación típica de un valor se obtiene restando la media y
dividiendo por la desviación típica de la muestra. """
# https://aprendeconalf.es/docencia/python/ejercicios/programacion-funcional/
from statistics import mean, stdev

def atypical_values(list):
    print(mean(list))
    print(stdev(list))
    list = sorted(list)
    median = round(len(list)/2)
    q1 = list[round((median/2))]
    q2 = list[median + round((median/2))]
    ric = q2 - q1
    #print(f'atipicos < {q1-1.5*ric} o > {q2+1.5*ric}')
    return [q1-3*ric, q1+3*ric]



print(atypical_values([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 1000]))