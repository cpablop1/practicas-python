""" Ejercicio 5
Escribir una función que reciba una frase y devuelva un diccionario
con las palabras que contiene y su longitud. """

def length(phrase):
    dictionary = {}
    for item in phrase.split():
        dictionary[item] = len(item)
    return dictionary

print(length('Este es una frase'))