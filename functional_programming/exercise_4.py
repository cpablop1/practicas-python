""" Ejercicio 4
Escribir una función que reciba otra función booleana y una lista, y devuelva otra
lista con los elementos de la lista que devuelvan True al aplicarles la función
booleana. """

def main(function, list):
    for item in range(len(list)-1, -1, -1):
        if not function(list[item]):
            list.pop(item)
    return list

def is_pair(number):
    if number%2 == 0:
        return True
    return False

print(main(is_pair, [2,3,6,9,8,10]))