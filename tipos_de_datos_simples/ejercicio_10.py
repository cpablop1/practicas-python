""" Ejercicio 10
Una juguetería tiene mucho éxito en dos de sus productos: payasos y muñecas.
Suele hacer venta por correo y la empresa de logística les cobra por peso de
cada paquete así que deben calcular el peso de los payasos y muñecas que saldrán
en cada paquete a demanda. Cada payaso pesa 112 g y cada muñeca 75 g. Escribir un
programa que lea el número de payasos y muñecas vendidos en el último pedido y
calcule el peso total del paquete que será enviado. """

def Pack():
    WEIGHT_CLOWNS = 112
    WEIGHT_DOLLS = 75
    num_clowns = input('Ingrese la cantidad de payasos: ')
    num_dolls = input('Ingrese la cantidad de muñecas: ')
    while (IsInt(num_clowns) & IsInt(num_dolls))!=True:
        if IsInt(num_clowns)!=True:
            num_clowns = input('Ingrese la cantidad de payasos (Eje: 2, 4, 9)')
        elif IsInt(num_dolls)!=True:
            num_dolls = input('Ingrese la cantidad de muñecas (Eje: 2, 4, 9)')
    weight_pack = (WEIGHT_CLOWNS*int(num_clowns)) + (WEIGHT_DOLLS*int(num_dolls))
    print(f'El peso total del paquete es de {str(weight_pack)} g')

def IsInt(number):
    try:
        int(number)
        return True
    except:
        return False
    
Pack()