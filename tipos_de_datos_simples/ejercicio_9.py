""" Ejercicio 9
Escribir un programa que pregunte al usuario una cantidad a invertir,
el interés anual y el número de años, y muestre por pantalla el capital
obtenido en la inversión. """

def Investment():
    amount = input('Ingrese la cantidad a invertir: ')
    interest = input('Ingrese el intirés anual: ')
    year = input('Ingrese la cantidad de años: ')
    while (IsFloat(amount) and IsFloat(interest) and IsInt(year))!=True:
        if IsFloat(amount)!=True:
            amount = input('Ingrese la cantidad a invertir (eje: 500, 530.50)')
        elif IsFloat(interest)!=True:
            interest = input('Ingrese el intirés anual (eje: 500, 530.50)')
        elif IsInt(year)!=True:
            year = input('Ingrese la cantidad de años (eje: 5, 7, 10)')
    total = (float(amount)*float(interest))*float(year)
    print(f'El capital obtenido en la inversión es de Q. {str(total)}')

def IsInt(number):
    try:
        int(number)
        return True
    except:
        return False
    
def IsFloat(number):
    try:
        float(number)
        return True
    except:
        return False
    
Investment()