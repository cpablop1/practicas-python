""" Ejercicio 6
Escribir un programa que lea un entero positivo, n, 
introducido por el usuario y después muestre en pantalla la suma de todos los enteros desde 1 hasta n. 
La suma de los n primeros enteros positivos puede ser calculada de la siguiente forma 
suma = n(n+1)/2"""

def Sumar():
    numero = int(input('Introduzca un número: '))
    suma = (numero*(numero+1))/2
    print(f'La sumatoria del 1 al {numero} es {suma}')

Sumar()