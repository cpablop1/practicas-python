""" Ejercicio 8
Escribir un programa que pida al usuario dos números enteros y muestre por
pantalla la <n> entre <m> da un cociente <c> y un resto <r> donde <n> y <m> 
son los números introducidos por el usuario, y <c> y <r> son el cociente y 
el resto de la división entera respectivamente. """

def Division():
    n = input('Ingrese el primer número: ')
    m = input('Ingrese el segundo número: ')
    c = 0
    r = 0
    while (IsInt(n)!=True) | (IsInt(m)!=True):
        if IsInt(n) != True:
            n = input('El primer número debe ser un entero: ')
        elif IsInt(m) != True:
            m = input('El segundo número debe ser un entero: ')
    c = int(int(n) / int(m))
    r = int(n) % int(m)
    print(f'El cociente de la división es {str(c)}')
    print(f'Y el resto es {str(r)}')

def IsInt(arg):
    try:
        int(arg)
        return True
    except:
        return False

Division()