""" Ejercicio 12
Una panadería vende barras de pan a 3.49€ cada una. El pan que no es
el día tiene un descuento del 60%. Escribir un programa que comience
leyendo el número de barras vendidas que no son del día. Después el
programa debe mostrar el precio habitual de una barra de pan, el
descuento que se le hace por no ser fresca y el coste final total. """

def CalculateDiscount():
    PRICE = 3.49
    DISCOUNT = 0.6
    number_bread = input('Ingrese la cantidad de pan vendidas que no son frescas: ')
    while IsInt(number_bread)!=True:
        number_bread = input('Ingrese la cantidad de pan vendidas que no son frescas: ')
    total = round(PRICE * float(number_bread), 2)
    subtotal = round(total - (total * DISCOUNT), 2)
    print(f'Los {number_bread} panes a Q. {str(PRICE)} cada una suman Q. {str(total)}.')
    print(f'Ahora aplicando el descuento del 60% da un coste final de Q. {str(subtotal)}.')

def IsInt(number):
    try:
        int(number)
        return True
    except:
        return False
    
CalculateDiscount()