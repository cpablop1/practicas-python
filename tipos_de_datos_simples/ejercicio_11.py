""" Ejercicio 11
Imagina que acabas de abrir una nueva cuenta de ahorros que te ofrece el 4%
de interés al año. Estos ahorros debido a intereses, que no se cobran hasta
finales de año, se te añaden al balance final de tu cuenta de ahorros. Escribir
un programa que comience leyendo la cantidad de dinero depositada en la cuenta
de ahorros, introducida por el usuario. Después el programa debe calcular y
mostrar por pantalla la cantidad de ahorros tras el primer, segundo y tercer
años. Redondear cada cantidad a dos decimales. """

def SavingsAmount():
    INTEREST = 0.04
    money_amount = input('Ingrese la cantidad existente en tu cuenta de ahorros: ')
    while IsFloat(money_amount)!=True:
        money_amount = input('Ingrese la cantidad existente en tu cuenta de ahorros (Eje: 5000, 1500 ...) ')
    money_amount = float(money_amount)
    for item in range(1, 4):
        interest = round(money_amount * (1+INTEREST), 2)
        print(f'El interés de {money_amount} generado durante {item} año son Q. {str(interest)}')
        money_amount = interest

def IsFloat(number):
    try:
        float(number)
        return True
    except:
        return False
    
SavingsAmount()