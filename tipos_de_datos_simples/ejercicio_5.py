""" Ejercicio 5
Escribir un programa que pregunte al usuario por el número de horas trabajadas y 
el coste por hora. Después debe mostrar por pantalla la paga que le corresponde. """

def CalcularPago():
    horas_trabajadas = input('Ingrese las horas trabajadas: ')
    coste_por_horas = input('Ingrese el coste por horas: ')
    total_a_pagar = int(horas_trabajadas) * float(coste_por_horas)

    print(f'El total a pagar es: {round(total_a_pagar, 2)}')

CalcularPago()