import pandas as pd

def student_score(score: dict) -> str:
    score = pd.Series(score)
    format_data = pd.Series(
        [round(score.min(),2),round(score.max(),2),round(score.mean(),2),round(score.std(),2)],
        index=['Nota Mínima:','Nota Máxima:','Media:','Desviación Típica:']
    )
    return format_data

print(student_score({'Juan':9, 'María':6.5, 'Pedro':4, 'Carmen': 8.5, 'Luis': 5}))