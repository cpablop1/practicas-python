import pandas as pd

def reports() -> None:
    sales = {}
    year = input('Ingrese el rango de años separados por comas (,) (Eje: 2020,2021): ').split(',')
    year = [int(item) for item in year]
    for item in range(year[0], year[1]+1):
        sales[item] = float(input(f'Ingrese el valor de la venta en el año {item}: '))
    print('\n\tVentas sin descuentos\n',pd.Series(sales))
    print('\n\tVentas con descuentos\n', pd.Series(sales)*0.10)

reports()