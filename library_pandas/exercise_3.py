import pandas as pd

def passed_failed(score: dict) -> str:
    #score = dict(sorted(score.items(), key=lambda x: x[1], reverse=True)) # Ordenamos el diccionario de mayor a menor
    score = pd.Series(score) # Convertimos la lista en un objeto Series
    return score[score>=5].sort_values(ascending=False) # Devolvemos la Serie filtrada segun las notas mayor o igual a 5

print(passed_failed({'Juan':9, 'María':6.5, 'Pedro':4, 'Carmen': 8.5, 'Luis': 5}))