import pandas as pd

def reports(data, month):
    data['Balance'] = data['Ventas']-data['Gastos']
    print(data.to_markdown(index=False))
    return data[data.Mes.isin(month)].Balance.sum()

print(reports(pd.DataFrame({
        'Mes': ['Enero','Febrero','Marzo','Abril'],
        'Ventas': [30500,35600,28300,33900],
        'Gastos': [22000,23400,18100,20700]
    }),['Enero','Marzo']))