# https://aprendeconalf.es/docencia/python/ejercicios/ficheros/
import os

def main() -> None:
    list = get_files('files/calificaciones.csv') # Función que recibe un archivo y devuelve una lista de diccionarios
    final_score = final_scores(list) # función que recibe la lista anterior y devulve una lista de diccionarios con la nota final
    passe_failed = passed_failed(final_score) # Función que recibe la lista anterior y devuelve 2 listas con estudiantes aprobrados y reprobados
    print(passe_failed)

def get_files(route: str) -> list:
    file, info = '', []
    if os.path.exists(route): # checamos si existe el archivo
        file = open(route, 'r').read().split('\n') # Leemos el archivo y la combertimos a una lista
        del file[0] # Eliminamos el primer elemento 
        del file[len(file) -1] # y el último elemento de la lista
        for item in file: # Recorremos cada elemento de la lista
            item = item.split(';') # Cada elemento de la lista la comvertirmos en una lista
            info.append( # Luego accedemos a cada indece la lista y creamos un diccionario con estos datos
                {
                    'Apellido':item[1],
                    'Nombre':item[0],
                    'Asistencia':item[2],
                    'Parcial1':format_score(item[3]),
                    'Parcial2':format_score(item[4]),
                    'Ordinario1':format_score(item[5]),
                    'Ordinario2':format_score(item[6]),
                    'Practicas':format_score(item[7]),
                    'OrdinarioPracticas':format_score(item[8])
                }
            )
    else:
        print('\nNo existe el directorio.\n')
    return info # Devolvemos la lista de diccionarios

def final_scores(list: list) -> list:
    parcial1 = parcial2 = practica = 0
    for item in list: # Recorremos la lista que se recibe por parámetros
        if item['Ordinario1'] == 0:
            parcial1 = item['Parcial1']
        else:
            parcial1 = item['Ordinario1']
        if item['Ordinario2'] == 0:
            parcial2 = item['Parcial2']
        else:
            parcial2 = item['Ordinario2']
        if item['OrdinarioPracticas'] == 0:
            practica = item['Practicas']
        else:
            practica = item['OrdinarioPracticas']
        item['NotaFinal'] = round((parcial1*0.30) + (parcial2*0.30) +(practica*0.40), 2) # Agregamos la nota final a cada diccionario de la lista
    return list # Y devolvemos lista de diccionarios con la nota final

def passed_failed(list: list) -> list:
    passed, failed = [], []
    final_score = asistencia = 0
    for item in list: # Recorremos la lista que se recibe por parámetro
        asistencia = int(item['Asistencia'].replace('%', '')) >= 75
        final_score = item['NotaFinal'] >= 5
        if asistencia & final_score: # Según la condición colocamos cada diccionario en la lista que corresponde
            passed.append(item)
        else:
            failed.append(item)
    return passed,failed # Devolmemos 2 listas de alumnos reprobados y aprobados

def format_score(score: str) -> float: # Función que sirve para formatear los números (notas)
    if len(score.strip()) == 0: # Si el parámetro es vacío devolvemos 0
        return 0
    else:
        score = score.replace(',', '.') # De lo cantrario reemplazamos la coma por el punto
        try:
            return int(score) # Luego devolvemos un entero
        except:
            return float(score) # o un número flotante

main()