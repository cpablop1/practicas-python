""" Ejercicio 2
Escribir una función que pida un número entero entre 1 y 10, lea el
fichero tabla-n.txt con la tabla de multiplicar de ese número, done
n es el número introducido, y la muestre por pantalla. Si el fichero
no existe debe mostrar un mensaje por pantalla informando de ello. """
import os

def read_files(nubmer):
    name = f'files/Table-{nubmer}.txt'
    if os.path.exists(name):
        print(open(name).read())
    else:
        print('No existe el archivo...')

read_files(5)