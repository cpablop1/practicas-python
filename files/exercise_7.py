""" Ejercicio 7
El fichero cotizacion.csv contiene las cotizaciones de las empresas del IBEX35 con las
siguientes columnas: Nombre (nombre de la empresa), Final (precio de la acción al
cierre de bolsa), Máximo (precio máximo de la acción durante la jornada), Mínimo
(precio mínimo de la acción durante la jornada), Volumen (Volumen al cierre de bolsa),
Efectivo (capitalización al cierre en miles de euros).

1. Construir una función reciba el fichero de cotizaciones y devuelva un
   diccionario con los datos del fichero por columnas.

2. Construir una función que reciba el diccionario devuelto por la función
   anterior y cree un fichero en formato csv con el mínimo, el máximo y la
   media de dada columna. """
import os
import statistics

def get_file(name_file: str) -> dict:
    file = f'files/{name_file}'
    dictionary = {'Nombres':[],'Final':[],'Máximo':[],'Mínimo':[],'Volumen':[],'Efectivo':[],}
    if os.path.exists(file):
        file = open(file, 'r').read().split('\n')
        del file[len(file)-1]
        del file[0]
        for item in file:
            item = item.split(';')
            dictionary['Nombres'].append(item[0])
            dictionary['Final'].append(item[1])
            dictionary['Máximo'].append(item[2])
            dictionary['Mínimo'].append(item[3])
            dictionary['Volumen'].append(item[4])
            dictionary['Efectivo'].append(item[5])
        return dictionary
    else:
        return f'\nNo hay ningún archivo con el nombre {name_file}\n'

def create_csv(dictionary: dict) -> None:
    file_csv = ''
    route = 'files/Datos_limpios.csv'
    clean_data = 'Nombres;Máximo;Mínimo;Media\n'
    if os.path.exists(route):
        file_csv = open(route, 'w')
    else:
        file_csv = open(route, 'x')
        file_csv = open(route, 'w')
    for key, value in enumerate(zip(dictionary['Máximo'],dictionary['Mínimo'])):
        nombres = dictionary['Nombres']
        value1 = float(value[0].replace(',','.'))
        value2 = float(value[1].replace(',','.'))
        clean_data += f'{nombres[key]};{value1};{value2};{statistics.mean([value1,value2])}\n'
    file_csv.write(clean_data)
    return

def main():
    dictionary = get_file('cotizacion.csv')
    create_csv(dictionary)

main()