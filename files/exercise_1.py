""" Ejercicio 1
Escribir una función que pida un número entero entre 1 y 10 y guarde
en un fichero con el nombre tabla-n.txt la tabla de multiplicar de
ese número, done n es el número introducido. """

import os
def multiplication_table(number):
    file = ''
    if os.path.exists(f'files/Table-{number}.txt'):
        file = open(f'files/Table-{number}.txt', 'a')
    else:
        file = open(f'files/Table-{number}.txt', 'x')
    file.write(f'\nTabla del {number}')
    for item in range(1, 11):
        file.write(f'\n{number} X {item} = {number*item}')

multiplication_table(5)