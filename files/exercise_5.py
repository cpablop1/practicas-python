""" Ejercicio 5
Escribir un programa que abra el fichero con información sobre el PIB
per cápita de los países de la Unión Europea
(url:https://ec.europa.eu/eurostat/estat-navtree-portlet-prod/BulkDownloadListing?file=data/sdg_08_10.tsv.gz&unzip=true),
pregunte por las iniciales de un país y muestre el PIB per cápita de
ese país de todos los años disponibles. """

# https://aprendeconalf.es/docencia/python/ejercicios/ficheros/

def pib(country: str) -> str:
    file = open('files/sdg_08_10.tsv', 'r')
    list_pib = file.read().split('\n')
    search = next((text for text in list_pib if country in text), None)
    if search is not None:
        year = list_pib[0].split('\t')
        pib = search.split('\t')
        del year[0]
        del pib[0]
        print('\n{:10} {}'.format('Año', 'PIB'))
        for year,pib in zip(year,pib):
            print('{:10} {}'.format(year,pib))
    else:
        print('\nNo existe el país en la lista\n')

pib('ES')