""" Ejercicio 3
Escribir una función que pida dos números n y m entre 1 y 10, lea el fichero
tabla-n.txt con la tabla de multiplicar de ese número, y muestre por pantalla
la línea m del fichero. Si el fichero no existe debe mostrar un mensaje por
pantalla informando de ello. """

# https://aprendeconalf.es/docencia/python/ejercicios/ficheros/
import os

def read_files(n: int, m: int) -> str:
    route = f'files/Table-{n}.txt'
    if os.path.exists(route):
        lines = open(route).readlines()
        try:
            index = lines.index(f'{n} X {m} = {n*m}\n')
            line = lines[index]
        except ValueError:
            return '\nNo existe la línea buscada...\n'
        return f'\n{line}'
    else:
        return f'\nNo existe ningún archivo con la tabla del {n}\n'
    
print(read_files(5,9))