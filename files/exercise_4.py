""" Ejercicio 4
Escribir un programa que acceda a un fichero de internet mediante su
url y muestre por pantalla el número de palabras que contiene. """
#from urllib import request
import requests

def read_internet_file(url: str) -> int:
    test = requests.get(url)
    content = test._content.split()
    return len(content)

print(read_internet_file("https://drive.google.com/file/d/1-IJNm_y849XwoomlJQ_bIRpx9KK0YE1w/view?usp=sharing"))