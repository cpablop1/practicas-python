""" Ejercicio 6
Escribir un programa para gestionar un listín telefónico con los nombres y
los teléfonos de los clientes de una empresa. El programa incorporar
funciones crear el fichero con el listín si no existe, para consultar el
teléfono de un cliente, añadir el teléfono de un nuevo cliente y eliminar
el teléfono de un cliente. El listín debe estar guardado en el fichero de
texto listin.txt donde el nombre del cliente y su teléfono deben aparecer
separados por comas y cada cliente en una línea distinta. """
import os

def listin():
    option = ''
    while option != '4': # Condición para salir del sistema
        route = 'files/listin.txt' # Obtenemos la ruta del archivo
        if check_directory(route):
            option = menu()
            if option == '1':
                name = input('\nIngrese nombre: ')
                phone = input('Ingrse número telefónico: ')
                add(name,phone,route)
            elif option == '2':
                name = input('\nIngrese el nombre: ')
                print(search(name,route))
            elif option == '3':
                name = input('\nIngrese el nombre: ')
                print(delete(name,route))
        else:
            print('\nNo existe el fichero\n')
            break
    print('\n\t\tSesión cerrada, vuelve pronto.\n')

def menu() -> str:
    option = input('\n1 Añadir teléfono' + 
                   '\n2 Consultar teléfono' +
                   '\n3 Eliminar teléfono' +
                   '\n4 Salir' +
                   '\nIngrese la opción: '
                   )
    return option

def check_directory(route) -> bool:
    if not os.path.exists(route): # Comprobamos si existe el archivo listin.txt
        try:
            open(route, 'x') # Si no existe la creamos
            return True
        except:
            return False
    else:
        return True

def add(name: str, phone: str, route: str) -> 0:
    file = open(route, 'a') # Con open() alcedemos al archivo y con el parámetro agregar "a" añadimos una nueva linea de texto
    file.write(f'{name},{phone}\n') # Agregamos los datos ingresados por el usuario al listin.txt
    file.close()

def search(name: str, route: str) -> str:
    file = open(route, 'r').read().split('\n') # Accedemos al listin.txt con el metodo read() y la covertimos a una lista
    search = next((text for text in file if name in text), None) # Buscamos el dato ingresado por el usuario en file y la guardamos en una variable
    result = ''
    if search is not None: # Si existe el dato
        search = search.split(',') # Lo convertimos en una lista
        result = f'\nEl cliente {search[0]} tiene el número telefónico {search[1]}' # Luego se lo mostramos en terminal
    else:
        result = f'\nNo existe ningún cliente con el nombre "{name}"\n'
    return result

def delete(name: str, route: str) -> str:
    file = open(route, 'r').read().split('\n') # Accedemos al listin.txt con el metodo read() y la covertimos a una lista
    search = next((text for text in file if name in text), None) # Buscamos el dato ingresado por el usuario en file y la guardamos en una variable
    result = ''
    if search is not None: # Si existe el dato
        index = file.index(search) # Buscamos el indice en la lista file, esto retorna un número
        del file[index] # Luego elminamos ese elemento de la lista
        update = '\n'.join(file) # Ahora volvemos a convertir esa lista actualizada a texto
        file = open(route, 'w').write(update) # Y finalmente actualizados el fichero listin.txt con "w" para para reemplazar el texto existente
        result = f'\nEl cliente {name} se ha eliminado correctamente de la lista\n'
    else:
        result = f'\nNo existe ningún cliente con el nombre "{name}"\n'
    return result
    
listin()