""" Ejercicio 4
Escribir un programa que pregunte al usuario los números ganadores
de la lotería primitiva, los almacene en una lista y los muestre
por pantalla ordenados de menor a mayor. """

def primitive_lottery():
    winning_numbers = []
    for item in range(6):
        number = input('Ingrese el número ganador: ')
        while is_valid(number)!=True:
            number = input('El número debe estar entre 1 y 49 ')
        winning_numbers.append(int(number))
    winning_numbers.sort()
    print(f'\nLos números ganadores son: {str(winning_numbers)}\n')

def is_valid(number):
    try:
        if (int(number) > 0) & (int(number) < 50):
            return True
        else:
            return False
    except:
        return False
    
primitive_lottery()