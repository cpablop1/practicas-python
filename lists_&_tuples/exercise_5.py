""" Ejercicio 5
Escribir un programa que almacene en una lista los números del
1 al 10 y los muestre por pantalla en orden inverso separados
por comas. """

def reverse():
    numbers = []
    for item in range(1, 11):
        numbers.append(item)
    numbers.reverse()
    print(str(numbers).replace('[', '').replace(']', ''))

reverse()