""" Ejercicio 6
Escribir un programa que almacene las asignaturas de un curso
(por ejemplo Matemáticas, Física, Química, Historia y Lengua)
en una lista, pregunte al usuario la nota que ha sacado en
cada asignatura y elimine de la lista las asignaturas aprobadas.
Al final el programa debe mostrar por pantalla las asignaturas
que el usuario tiene que repetir. """

def rubric():
    subjects = ['Matemática','Física','Química','Historia','Lengua']
    rubric = []
    approved = []
    for item in range(5):
        score = input(f'Ingrese la nota de {subjects[item]}: ') #Se solicita la nota de la asignatura
        while is_valid(score)!=True: # Se comprueba si la nota es válida
            score = input('La nota debe estar entre 1 y 100: ')
        rubric.append(int(score)) # Agregamos la nota a la lista
        if int(score)>59: # Comprobamos si aprobó el asignatura
            approved.append(item) # Agregamos el índice de la asignatura aprobada a la lista approved
    for item in sorted(approved, reverse=True): # Recorremos la lista de las asignatura aprobadas
         subjects.pop(item) # Eliminamos la asignatura aprobada
         rubric.pop(item) # También eliminamos la nota de la asignatura aprobada
    print('\n##### Cursos que debe repetir #####')
    # Imprimimos las asignaturas reprobadas
    if len(subjects) == 0: # Primero debemos comprobar si ha ya asiguras que mostrar
        print('Felicidades, has aprobado todos los cursos!')
    else:
        for item in range(len(subjects)):
            print(f'{subjects[item]}: {rubric[item]}')

def is_valid(number):
    try:
        if (int(number) > 0) & (int(number) <= 100):
            return True
        else:
            return False
    except:
        return False
    
rubric()