""" Ejercicio 2
Escribir un programa que almacene las asignaturas de un curso
(por ejemplo Matemáticas, Física, Química, Historia y Lengua)
en una lista y la muestre por pantalla el mensaje Yo estudio
<asignatura>, donde <asignatura> es cada una de las asignaturas
de la lista. """

def I_study():
    subjects = ['Matemáticas','Física','Química','Historia','Lengua']
    for item in subjects:
        print(f'Yo estudio {item}')

I_study()