""" Ejercicio 7
Escribir un programa que almacene el abecedario en una lista, elimine
de la lista las letras que ocupen posiciones múltiplos de 3, y
muestre por pantalla la lista resultante. """

def alphabet():
    alphabet = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','ñ','o','p','q','r','s','t','u','v','w','x','y','z']
    for item in range(len(alphabet), 1, -1):
        if item%3 == 0:
            alphabet.pop(item-1)
    print(alphabet)

alphabet()