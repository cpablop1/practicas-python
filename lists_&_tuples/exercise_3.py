""" Ejercicio 3
Escribir un programa que almacene las asignaturas de un curso
(por ejemplo Matemáticas, Física, Química, Historia y Lengua)
en una lista, pregunte al usuario la nota que ha sacado en cada
asignatura, y después las muestre por pantalla con el mensaje En
<asignatura> has sacado <nota> donde <asignatura> es cada una des
las asignaturas de la lista y <nota> cada una de las correspondientes
notas introducidas por el usuario. """

def rubric():
    subjects = ['Matemáticas','Física','Química','Historia','Lengua']
    rubric = []
    for item in subjects:
        grade = input(f'Ingrese la nota de {item}: ')
        rubric.append(grade)
    print('')
    for item in range(len(subjects)):
        print(f'En {subjects[item]} has sacado {rubric[item]}.')
    print('')

rubric()