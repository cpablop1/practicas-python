""" Ejercicio 10
Escribir un programa que almacene en una lista los siguientes
precios, 50, 75, 46, 22, 80, 65, 8, y muestre por pantalla
el menor y el mayor de los precios. """

def max_min():
    price = [50, 75, 46, 22, 80, 65, 8]
    min = max = price[0]
    for item in price:
        if item < min:
            min = item
        elif item > max:
            max = item
    print(f'El precio menor es {min}')
    print(f'Y el precio mayor es {max}')
    # Alternativa y simplicidad de codigo existen dos funciones (min() y max())
    """ print(f'El precio menor es {min(price)}')
    print(f'Y el precio mayor es {max(price)}') """

max_min()