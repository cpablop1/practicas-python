""" Ejercicio 12
Escribir un programa que almacene las matrices
 
 a = [[5,3,-4,-2],[8,-1,0,-3]]
 b = [[1,-5,0,5],[4,3,-9,1],[0,7,5,4]]

en una lista y muestre por pantalla su producto.
Nota: Para representar matrices mediante listas usar
listas anidadas, representando cada vector fila en una lista. """

def product_matrices():
    a = [[5,3,-4,-2],[8,-1,0,-3]]
    b = [[1,-5,0,5],[4,3,-9,1],[0,7,5,4]]
    c = []
    for item in range(len(a)):
        product = []
        for item0 in range(len(b)):
            product.append(sum(((i*j)) for i,j in zip(a[item],b[item0])))
        c.append(product)
    for item in c:
        print(item)

product_matrices()