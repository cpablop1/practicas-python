""" Ejercicio 11
Escribir un programa que almacene los vectores (1,2,3)
y (-1,0,2) en dos listas y muestre por pantalla su
producto escalar. """

def scalar_product():
    u = [1,2,3]
    v = [-1,0,2]
    product = 0
    for vector1,vector2 in zip(u,v):
        product += (vector1*vector2)
    print(product)

scalar_product()