""" Ejercicio 9
Escribir un programa que pida al usuario una palabra y muestre
por pantalla el número de veces que contiene cada vocal. """
import re

def count_vowel():
    word = input('Hola, escribe una palabra: ')
    vowel = input('Ahora escribe una vocal: ')
    list_vowel = ['a','e','i','o','u']
    count_vowel = 0
    while (vowel in list_vowel)!=True:
        vowel = input('Debe ser una vocal: ')
    for item in word:
        if vowel == item:
            count_vowel += 1
    print(f'La vocal "{vowel}" aparece {str(count_vowel)} veces en {word}')
    
count_vowel()