""" Ejercicio 13
Escribir un programa que pregunte por una muestra de números, separados
por comas, los guarde en una lista y muestre por pantalla su media y
desviación típica. """
#  9,3,8,8,9,8,9,18
import math

def stadistics():
    number = input('Ingrese la muestra de números: ')
    number = number.split(',')
    average = 0
    tipical_deviation = 0
    print(tuple(number))
    for item in number:
        average += int(item)
    average = (average/len(number))
    for item in number:
        tipical_deviation += (int(item)-average)**2
    tipical_deviation = (tipical_deviation/len(number))
    tipical_deviation = round(math.sqrt(tipical_deviation), 2)
    print(f'La media es {str(average)}')
    print(f'La desviación típica es {str(tipical_deviation)}')

stadistics()