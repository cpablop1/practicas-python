""" Ejercicio 4
Los teléfonos de una empresa tienen el siguiente formato prefijo-número-extension
donde el prefijo es el código del país +34, y la extensión tiene dos dígitos
(por ejemplo +34-913724710-56). Escribir un programa que pregunte por un número
de teléfono con este formato y muestre por pantalla el número de teléfono sin
el prefijo y la extensión. """
import re

def input_data():
    number = input('Ingrese el número de teléfono con el código del país +() y la extensión al final (Eje: +502-32375649-115): ')
    pattern = re.compile(r'\-(\d+)-')
    while is_valid(number)!=True:
        number = input('Ingrese el número de teléfono con el siguiente formato +502-32375649-115: ')
    phone = pattern.search(number).group(1)
    print(f'El número de teléfono sin el prefijo y la extesión es {phone}')

def is_valid(number):
    pattern = re.compile(r'^\+\d{1,}-\d{8}-\d{1,}$')
    if pattern.match(number):
        return True
    else:
        return False

input_data()