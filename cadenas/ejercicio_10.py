""" Ejercicio 10
Escribir un programa que pregunte por consola por los productos de una cesta
de la compra, separados por comas, y muestre por pantalla cada uno de los
productos en una línea distinta. """

def main():
    products = input('Ingrese el nombre de los productos separados por (,) ').split(',')
    for item in products:
        print('- ' + item.strip().title())

main()