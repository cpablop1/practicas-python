""" Ejercicio 2
Escribir un programa que pregunte el nombre completo del usuario en la
consola y después muestre por pantalla el nombre completo del usuario
tres veces, una con todas las letras minúsculas, otra con todas las letras
mayúsculas y otra solo con la primera letra del nombre y de los apellidos
en mayúscula. El usuario puede introducir su nombre combinando mayúsculas
y minúsculas como quiera. """

import re

def full_name():
    full_name = input('Ingrese su nombre completo: ')
    while is_valid(full_name)!=True:
        full_name = input('Ingrese un nombre válido por favor: ')

    print(full_name.lower())
    print(full_name.upper())
    print(full_name.title())


def is_valid(name):
    pattern=re.compile(r'^[A-Za-zÁáÉéÍíÓóÚúÜü]+(?: [A-Za-zÁáÉéÍíÓóÚúÜü]+)*$')
    if pattern.match(name):
        return True
    else:
        return False

full_name()