""" Ejercicio 9
Escribir un programa que pregunte al usuario la fecha de su nacimiento
en formato dd/mm/aaaa y muestra por pantalla, el día, el mes y el año.
Adaptar el programa anterior para que también funcione cuando el día o
el mes se introduzcan con un solo carácter. """
import re
from datetime import datetime
import locale

locale.setlocale(locale.LC_TIME, 'es_ES.UTF-8') #Configuracion al español

def date():
    date = input('Ingrese su fecha de nacimiento en este formato (dd/mm/aaa)') # Pedir los datos al usuario
    while is_valid(date)!=True: # LLamar la funcion y verificar si es una fecha valida
        date = input('El formato debe ser ésta (dd/mm/aaa) ')
    date = re.findall(r'\d+', date) # con una expresion regular extraer los numeros y guardarlos en un array
    date = datetime(int(date[2]), int(date[1]), int(date[0])) # Darle el formato de fecha
    print(date.strftime('%A %d de %B del %Y')) # imprir el resultado

# funcion para comprobar si la fecha introducida coincida
# con el formato solicitado
def is_valid(date):
    #con una expresion relgar hacemos valer el formato de la fecha
    pattern = re.compile(r'^(0[1-9]|[1-2][0-9]|3[0-1])/(0[1-9]|1[0-2])/\d{4}$')
    if pattern.match(date): # Evaluar si coincide con el formato y retornar True,
        return True
    else: # en caso contrario retorna falso
        return False
    
date()