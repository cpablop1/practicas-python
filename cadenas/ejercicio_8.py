""" Ejercicio 8
Escribir un programa que pregunte por consola el precio de un producto
en Quetzales con dos decimales y muestre por pantalla el número de Quetzales y
el número de céntimos del precio introducido. """

def print_price():
    price = input('Ingrese el precio del producto: ')
    while is_valid(price)[0]!=True:
        price = input('Ingrese un precio válido (Eje: 10, 35, 3.50): ')
    price = str(is_valid(price)[1])
    number_q = price[:price.find('.')]
    number_c = price[price.find('.')+1:]
    print(f'{number_q} quetzales y {number_c} centavos.')

def is_valid(price):
    try:
        float(price)
        return [True, float(price)]
    except:
        return [False, 0]
    
print_price()