""" Ejercicio 6
Escribir un programa que pida al usuario que introduzca una frase
en la consola y una vocal, y después muestre por pantalla la misma
frase pero con la vocal introducida en mayúscula. """
import re

def system():
    phrase = input('Hola, escribe una frase: ')
    vowel = input('Muy bien, ahora ingrese una vocal: ')
    while (is_valid(phrase)!=True) | (is_vowel(vowel)!=True):
        if is_valid(phrase)!=True:
            phrase = input('Ingrese una frase válida: ')
        elif is_vowel(vowel)!=True:
            vowel = input('Ingrese una vocal: ')
    replace = phrase.replace(vowel, vowel.upper())
    print(replace)

def is_valid(text):
    if(len(text.strip()) > 0):
        return True
    else:
        return False
    
def is_vowel(vowel):
    vowels = ['a', 'e', 'i', 'o', 'u']
    if vowel in vowels:
        return True
    else:
        return False

system()