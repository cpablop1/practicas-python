""" Ejercicio 1
Escribir un programa que pregunte el nombre del usuario en la consola
y un número entero e imprima por pantalla en líneas distintas el nombre
del usuario tantas veces como el número introducido. """

def loop():
    name = input('Hola, escribe tu nombre: ')
    number = input('Ahora escribe un número: ')
    while is_empty(name)!=True or is_int(number)!=True:
        if is_empty(name)!=True:
            name = input('Hola, escribe tu nombre: ')
        elif is_int(number)!=True:
            number = input('Ahora escribe un número: ')
    for item in range(int(number)):
        print(f'{item+1} - {name}')


def is_int(number):
    try:
        int(number)
        return True
    except:
        return False
    
def is_empty(text):
    empty = text.strip()
    if len(empty) != 0:
        return True
    else:
        return False
    
loop()