""" Ejercicio 7
Escribir un programa que pregunte el correo electrónico del usuario en la
consola y muestre por pantalla otro correo electrónico con el mismo nombre
(la parte delante de la arroba @) pero con dominio ceu.es. """
import re

def change_domain():
    email = input('Ingrese un correo electrónico: ').strip()
    while is_valid(email)!=True:
        email = input('Ingrese un correo electrónico válido (Eje: username@gmail.com): ')
    extract = email[0:email.find('@')+1]
    print(f'{extract}ceu.es')


def is_valid(email):
    pattern = re.compile(r'^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$')
    if pattern.match(email):
        return True
    else:
        return False
    
change_domain()