""" Ejercicio 5
Escribir un programa que pida al usuario que introduzca una frase
en la consola y muestre por pantalla la frase invertida. """
import re

def reverse_phrase():
    phrase = input('Hola, escribe una frase: ')
    while is_valid(phrase)!=True:
        phrase = input('Escribe una frase válida: ')
    search = re.findall(r'\b\w+\b', phrase)
    reverse = search[::-1]
    print(' '.join(reverse))

def is_valid(text):
    if(len(text.strip()) > 0):
        return True
    else:
        return False
    
reverse_phrase()