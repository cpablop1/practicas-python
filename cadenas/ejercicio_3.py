""" Ejercicio 3
Escribir un programa que pregunte el nombre del usuario en la consola
y después de que el usuario lo introduzca muestre por pantalla <NOMBRE>
tiene <n> letras, donde <NOMBRE> es el nombre de usuario en mayúsculas
y <n> es el número de letras que tienen el nombre. """

import re

def full_name():
    full_name = input('Ingrese su nombre completo: ')
    while is_valid(full_name)!=True:
        full_name = input('Ingrese un nombre válido por favor: ')

    print(f'{full_name.upper()}, tiene {len(full_name.replace(" ", ""))} letras.')


def is_valid(name):
    pattern=re.compile(r'^[A-Za-zÁáÉéÍíÓóÚúÜü]+(?: [A-Za-zÁáÉéÍíÓóÚúÜü]+)*$')
    if pattern.match(name):
        return True
    else:
        return False

full_name()