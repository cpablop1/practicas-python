listin = {'Juan':123456789, 'Pedro':987654321}

def elimina(listin, usuario):
    return listin.pop(usuario, f'\nNo existe ningún usuario con el nombre "{usuario}"\n')

print(elimina(listin, 'Pablo'))
